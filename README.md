# Website

Localverse Website

Temporary placeholder website is in branch placeholder-branch in folder placeholder

### Development Talk
https://discord.gg/8m6BGnEU 
### Development Location
Codeberg: https://codeberg.org/Localverse/Website

### Development Guidelines
- Use any programming language
- Code in a modular, easily extensible and re-usable fashion
- please comment your code
- If you have any Questions or Ideas please go to Development Talk
- Use webassembly for all interactive/dynamic features of the website
- - If you are implementing a feature, working on this project, please say what you are working on in the Dev Talk and take the issue for it on the repo. If there is no issue made for it please make one.
- Fork project or clone project, make your changes, test them, if they work, push to dev-branch, then open pull request to main-branch.

### To Do List
- go through project and come up with development plan
- FInish Work on README
- Add contributer guidelines
- add code of conduct
- add pull request guidelines
- add security

## Helpful Links
- 